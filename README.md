# OctaNifty

![OctaNifty preview screenshot](/doc/OctaNifty-preview.png)

Simple EmberJS / Octane (v3.17.0) demo using following addons:
 - ember-concurrency (1.1.7)
 - ember-leaflet (4.1.0)
 - tailwindcss (1.2.0)
 - ember-animated (0.10.0)
 - ember-fontawesome (0.2.1)

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](https://git-scm.com/)
* [Volta](https://volta.sh/)
* [Ember CLI](https://ember-cli.com/)

## Installation

* `git clone <repository-url>` this repository
* `cd OctaNifty`
* `yarn`

## Running / Development

* `yarn start`
* Visit your app at [http://localhost:4200](http://localhost:4200).
* Visit your tests at [http://localhost:4200/tests](http://localhost:4200/tests).

### Running Tests

* `ember test`
