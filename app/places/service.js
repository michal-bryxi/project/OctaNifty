import Service from '@ember/service';

export default class PlacesService extends Service {
  all = [
    { "name": "The Paramount Hotel Portland", "rating": 4.6, "lat": 45.5182273, "lng": -122.6815842 },
    { "name": "Headwaters Restaurant Portland", "rating": 4.2, "lat": 45.5171873, "lng": -122.6810931 },
    { "name": "Clyde Common", "rating": 4.3, "lat": 45.5220071, "lng": -122.6816181 },
    { "name": "Jake's Famous Crawfish", "rating": 4.5, "lat": 45.5225049, "lng": -122.6834922 },
    { "name": "Punch Bowl Social Portland", "rating": 4, "lat": 45.5180039, "lng": -122.6759926 },
    { "name": "Portland City Grill", "rating": 4.5, "lat": 45.52250730000001, "lng": -122.6761667 },
    { "name": "Southpark Seafood", "rating": 4.4, "lat": 45.5178601, "lng": -122.6822699 },
    { "name": "Saucebox", "rating": 4.1, "lat": 45.5223539, "lng": -122.6777143 },
    { "name": "Jake's Grill", "rating": 4.4, "lat": 45.5206041, "lng": -122.6823935 },
    { "name": "Huber's Cafe", "rating": 4.5, "lat": 45.520069, "lng": -122.674738 },
    { "name": "Buffalo Wild Wings", "rating": 3.9, "lat": 45.51853310000001, "lng": -122.6761788 },
    { "name": "Multnomah Whiskey Library", "rating": 4.7, "lat": 45.52098079999999, "lng": -122.6834594 },
    { "name": "Mary's Club", "rating": 3.7, "lat": 45.5227709, "lng": -122.6778636 },
    { "name": "Picnic House", "rating": 4.3, "lat": 45.517604, "lng": -122.6813022 },
    { "name": "Shigezo", "rating": 4.4, "lat": 45.517678, "lng": -122.6825653 },
    { "name": "Living Room Theaters", "rating": 4.5, "lat": 45.5222837, "lng": -122.6811793 },
    { "name": "Veggie Grill", "rating": 4.4, "lat": 45.5174139, "lng": -122.6785472 },
    { "name": "Higgins", "rating": 4.5, "lat": 45.515572, "lng": -122.682 },
    { "name": "Blue Star Donuts", "rating": 4.5, "lat": 45.5207156, "lng": -122.6838743 },
    { "name": "Masu Sushi", "rating": 4.5, "lat": 45.522484, "lng": -122.684079 }
  ];
}
