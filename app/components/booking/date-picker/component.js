import Component from '@glimmer/component';
import moment from 'moment';
import fade from 'ember-animated/transitions/fade';

export default class DatePickerComponent extends Component {
  transition = fade;

  get dates() {
    let now = moment();
    return [ ...Array(6).keys() ].map((i) => {
      let then = now.clone().add(i, 'days');
      return {
        dayName: i === 0 ? "Today" : (i === 1 ? "Tomorrow" : then.format('dddd')),
        date: then,
        isWeekend: then.format('d') % 6 === 0
      }
    })
  }
}
