import Component from '@glimmer/component';
import moment from 'moment';
import fade from 'ember-animated/transitions/fade';

export default class TimePickerComponent extends Component {
  transition = fade;

  get times() {
    if(!this.args.selectedDate || !this.args.selectedDaytime) {
      return [];
    }

    let INTERVAL = 20;
    let COUNT = 12;

    let now = this.args.selectedDate.date.clone().hour(this.args.selectedDaytime.start.hour).minute(0);
    return [ ...Array(COUNT).keys() ].map((i) => {
      return {
        start: now.clone().add(i * INTERVAL, 'minutes'),
        end: now.clone().add((i + 1) * INTERVAL, 'minutes')
      };
    })
  }
}
