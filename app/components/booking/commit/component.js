import Component from '@glimmer/component';
import fade from 'ember-animated/transitions/fade';

export default class BookingCommitComponent extends Component {
  transition = fade;
}
