import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { task } from 'ember-concurrency';
import { computed } from '@ember/object';

export default class BookingComponent extends Component {
  @service store;
  @tracked selectedDate = null;
  @tracked selectedDaytime = null;
  @tracked selectedTime = null;

  @computed('selectedDate', 'selectedDaytime', 'selectedTime')
  get selectedBooking() {
    if(!this.selectedDate || !this.selectedDaytime || !this.selectedTime) {
      return null;
    } else {
      return this.selectedTime;
    }
  }

  @action selectDate(date) {
    this.selectedDate = date;
  }

  @action selectDaytime(daytime) {
    this.selectedDaytime = daytime;
  }

  @action selectTime(time) {
    this.selectedTime = time;
  }

  @(task(function * () {
    let reservation = this.store.createRecord(
      'reservation',
      {
        from: this.selectedTime.start,
        to: this.selectedTime.to,
        email: "john.doe@example.com",
        placeId: "ChIJ-8S02RoKlVQRkQDQulm7Sjo"
      }
    );

    yield reservation.save();
  })) makeReservation;
}
