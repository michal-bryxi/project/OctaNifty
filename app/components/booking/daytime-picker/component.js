import Component from '@glimmer/component';
import moment from 'moment';
import fade from 'ember-animated/transitions/fade';

export default class DatetimePickerComponent extends Component {
  transition = fade;

  get daytimes() {
    const COUNT = 4;
    const WIDTH = 4;
    const NAMES = ['morning', 'noon', 'afternoon', 'evening'];

    let now = moment({hour: 6, minute: 0});
    return [ ...Array(COUNT).keys() ].map((i) => {
      return {
        name: NAMES[i],
        start: now.clone().add(i * WIDTH, 'hours'),
        end: now.clone().add((i + 1) * WIDTH, 'hours')
      };
    });
  }
}
