import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';

export default class MapComponent extends Component {
  @service places;

  lat = 45.519743;
  lng = -122.680522;
  zoom = 16;
}
