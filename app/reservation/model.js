import Model, { attr } from '@ember-data/model';

export default class ReservationModel extends Model {
  @attr('date') start;
  @attr('date') end;
  @attr('number') placeId;
  @attr('string') email;
}
