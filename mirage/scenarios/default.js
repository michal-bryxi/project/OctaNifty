export default function(server) {

  /*
    Seed your development database using your factories.
    This data will not be loaded in your tests.
  */

  server.createList('reservation', 10);
  server.createList('occupancy', 10);
}
