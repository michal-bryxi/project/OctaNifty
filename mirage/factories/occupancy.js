import { Factory } from 'ember-cli-mirage';
import faker from 'faker';

export default Factory.extend({
  start() {
    return faker.date.past();
  },

  end() {
    const copy = new Date(Number(this.start))
    copy.setHours(copy.getHours() + 1)
    return copy;
  },

  amount() {
    return faker.random.number({min: 0, max: 999});
  },
});
